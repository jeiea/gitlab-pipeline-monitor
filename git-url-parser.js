const { parse } = require('url');

module.exports = url => {
  const giturl = /\:\/\//.test(url) ? url : `ssh://${url.replace(/:~?/g, '/')}`;
  const { hostname, path, port } = parse(giturl);
  return {
    domain: hostname,
    project: path
      .replace('.git', '')
      .replace(/^\/\/?/, '')
      .trim()
  };
};
