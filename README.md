[![Version](https://vsmarketplacebadge.apphb.com/version-short/balazs4.gitlab-pipeline-monitor.svg?style=flat-square)](https://marketplace.visualstudio.com/items?itemName=balazs4.gitlab-pipeline-monitor)
[![Rating](https://vsmarketplacebadge.apphb.com/rating-star/balazs4.gitlab-pipeline-monitor.svg?style=flat-square)](https://marketplace.visualstudio.com/items?itemName=balazs4.gitlab-pipeline-monitor)
[![Installs](https://vsmarketplacebadge.apphb.com/installs-short/balazs4.gitlab-pipeline-monitor.svg?style=flat-square)](https://marketplace.visualstudio.com/items?itemName=balazs4.gitlab-pipeline-monitor)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)

# gitlab-pipeline-monitor

> This extension monitors your latest GitLab Pipeline on the current branch

## Features

- shows the latest state of the pipeline in the statusbar

  - ![pending](.assets/pending.png)
  - ![running](.assets/running.png)
  - ![success](.assets/success.png)
  - ![failed](.assets/failed.png)

- statusbar item is clickable, opens the pipeline view in the default browser

## Requirements

The extension gets activated only if the workspace contains a `.gitlab-ci.yml` file.

## Extension Settings

> if you are using the [previous config format](https://gitlab.com/balazs4/gitlab-pipeline-monitor/blob/1.0.6/README.md#extension-settings), it is highly **recommended** to manually migrate it to the new format. The old config should work, but it is not supported anymore.

You can configure the settings per gitlab deployment.
There is only one mandatory parameter in each section: `token`.
Any other parameter has a default/fallback value if you do not define it.

```json
"gitlabPipelineMonitor": {
   "<origin-domain>": {
     "token": "<my-private-token>",                // MANDATORY
     "notifyOnFailed": false,                      // optional, default: false
     "apiUrl": "https://<origin-domain>/api/v4",   // optional, default: https://<origin-domain>/api/v4
     "interval": 10000                             // optional, default: 10000 milliseconds
   },
   "gitlab.com" : {
     "token": "gitlab-private-token"
   },
   "my.internal.gitlab.instance.com:29418": {
     "token": "my-internal-token",
     "apiUrl": "https://my.internal.gitlab.instance.com/api/v4"
   },
   "origin-is-not-even-a-gitlab-deployment": {
     "token": "my-token-to-gitlab.com",
     "apiUrl": "https://gitlab.com/api/v4"
   }
},
```

> how to create [Gitlab Personal Access Token](https://docs.gitlab.com/ce/user/profile/personal_access_tokens.html) with `api` scope

**Hint**: If you are using private gitlab deployment with self-signed cert, you should set the `http.proxyStrictSSL` to `false`. More details: <https://gitlab.com/balazs4/gitlab-pipeline-monitor/issues/3>

## Known Issues

Currently no `multi-workspace` support.

## Author

balazs4

- <https://twitter.com/balazs4>
- <https://gitlab.com/balazs4>
- <https://github.com/balazs4>

## Release Notes

<https://gitlab.com/balazs4/gitlab-pipeline-monitor/tags>
